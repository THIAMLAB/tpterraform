### Variables
variable front_instance_number {
  default = "2"
}

variable front_ami {
  default = "ami-0d77397e" # Ubuntu 16.04
}

variable front_instance_type {
  default = "t2.micro"
}

variable public_key {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDUnkjY684IdkJK5s5khkV9VijpUpwArZ4eZTfYPzQZO6eFsiQ+lFPMuR7tKJzNGt0WtyKUcmwX/e5n9e8F+aRPVTay/oB+DRBEkHE04wyouQhRKNT4Ja9Dn+5RaUEX2V7/YIZGMcCcHGZaF37bUGz384KNJwdx4hL8YVL9/Pu35FFYi7wBSKJruAIlKZhmnEcaDDE/htArH5Ajre/N5kwzgACEAa6czVSQj7/KQPDDYC6LBiY8pD795rfNzUT7ULhuMs7irtUx9Qn1vyvGz5l2fjtphmv7dLELI8aOtIHsPb3biVjwQvrG0fUIzGgFYpkqrBUM5UwnSf+A+bTv8Gyx/z+L9daSm11JLAWJ81XDtPG+mSWYcWdzIzWSDPEB8FNbw2lq6+r92VhiKJHCwhyA1JEhGng82UpHm20BHSyuhj4sm6EqVRxJYFYM8+j+uIsgXtMfpLqapp/qmRf8EYVz50SAqUsZxazUHwrmO2KK01oSxUKS4I3C+xy2uiGgMiDi4bfDoIlBYJy1r7JeAYBr62ZRIKNgcOZSegLiCCtUlx5gi1/wcXUq9q0VLHll3BvyHd/cZtCM4HtX3KofQdaHpWi+GgerDC/QxYfxVJsgm9AIly4cTfx70BO+0RXzS17cWLjnU3FPV4fw2TD4wB3HiOF+Eo0LT74nbmyXnD+6Xw== ybellal@D2SIWKS079"
}

variable front_elb_port {
  default = "80"
}

variable front_elb_protocol {
  default = "http"
}

### Resources
resource "aws_key_pair" "front" {
  key_name   = "${var.project_name}-front"
  public_key = "${var.public_key}"
}

resource "aws_instance" "front" {
  # TO DO
  # see https://www.terraform.io/docs/providers/aws/r/instance.html
  count                  = "${var.front_instance_number}"
  ami                    = "${var.front_ami}"
  instance_type          = "${var.front_instance_type}"
  subnet_id              = "${aws_subnet.public.*.id[count.index]}"
  vpc_security_group_ids = ["${aws_security_group.front.id}"]
  key_name               = "${aws_key_pair.front.key_name}"

  tags = {
    Name = "${var.project_name}-front-${count.index}"
  }
}

resource "aws_elb" "front" {
  # TO DO
  # see https://www.terraform.io/docs/providers/aws/r/elb.html
  name            = "${var.project_name}-front-elb"
  subnets         = ["${aws_subnet.public.*.id}"]
  security_groups = ["${aws_security_group.elb.id}"]
  instances       = ["${aws_instance.front.*.id}"]

  listener {
    instance_port     = "${var.front_elb_port}"
    instance_protocol = "${var.front_elb_protocol}"
    lb_port           = "${var.front_elb_port}"
    lb_protocol       = "${var.front_elb_protocol}"
  }

  tags = {
    Name = "${var.project_name}-front-elb"
  }
}

### Outputs
output "elb_endpoint" {
  # TO DO
  # see https://www.terraform.io/intro/getting-started/outputs.html
  value = "${aws_elb.front.dns_name}"
}

output "instance_ip" {
  # TO DO
  value = ["${aws_instance.front.*.public_ip}"]
}
